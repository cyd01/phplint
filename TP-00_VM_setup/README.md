# TP-01: Installation d'une machine virtuelle pour maquettage

## Création de la VM

```shell
vagrant box update
vagrant up
```

## Connection en console

```shell
vagrant ssh
```

## Installation de docker

```shell
curl -fsSL https://get.docker.com -o - | sudo bash
sudo usermod -aG docker vagrant
```

Se déloguer et se re-loguer pour que les modifications de groupes soient effectives.

## Vérifications

```shell
docker info
```

## docker-compose

```shell
export DEBIAN_FRONTEND=noninteractive && \
sudo apt-get update && \
sudo apt-get install -y docker-compose
```

```shell
sudo curl -sSL https://raw.githubusercontent.com/docker/compose/1.23.2/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose
```

## docker-machine

```shell
base=https://github.com/docker/machine/releases/download/v0.16.0 && \
curl -sSL $base/docker-machine-$(uname -s)-$(uname -m) >/tmp/docker-machine && \
sudo mkdir -p /usr/local/bin && \
sudo install /tmp/docker-machine /usr/local/bin/docker-machine
```

```shell
base=https://raw.githubusercontent.com/docker/machine/v0.16.0 && \
for i in docker-machine-prompt.bash docker-machine-wrapper.bash docker-machine.bash
do
  sudo wget "$base/contrib/completion/bash/${i}" -P /etc/bash_completion.d
done
```
