# TP 06: docker volumes

1. Utilisez docker-compose pour lancer une stack tirant partie des 2 scripts dans le répertoire src:

- producer.sh: produit des fichier horodatés dans /data/in
- worker.sh: traite les fichiers horodatés dans /data/in, puis les déplace vers /data/out

2. Couper votre stack aprés l'avoir laissé tourner 5 minutes. Avez vous perdu vos données ? Si non comment les retrouver ? Pouvez vous en faire une sauvegarde ?

3. Relancer la même stack en y incluant le script supplémentaire fournit:

- watcher.sh: supervise le tout en donant le nombre de fichiers dans cachun des répertoires

Vous pouvez à présent surveiller l'activité au fil de l'eau.

4. Pour aller plus loin essayer d'utiliser des data-containers, ce qui est une pratique presque obsolète mais des fois utile.