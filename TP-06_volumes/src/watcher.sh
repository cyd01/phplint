#!/bin/ash

while true; do
    echo -n "`date +%H%M%S`: /data/in : "
    ls -1 /data/in | wc -l
    echo -n "`date +%H%M%S`: /data/out : "
    ls -1 /data/out | wc -l
    sleep 1
done