#!/bin/ash

[ ! -d /data/in ] && mkdir -p /data/in
[ ! -d /data/out ] && mkdir -p /data/out

while true; do
    FLIST=`find /data/in -name '*.dat' -exec realpath {} \;`
    SLEEPTIME=`cat /dev/urandom | tr -dc '0-9' | fold -w 256 | head -n 1 | head -c 1`
    for file in $FLIST; do
        echo -n "`date +%H%M%S` - moving $file"
        mv $file /data/out/
        echo " ."
    done
    sleep $SLEEPTIME
done