#!/bin/ash

[ ! -d /data/in ] && mkdir -p /data/in
[ ! -d /data/out ] && mkdir -p /data/out

while true; do
    EDATE=`date +%H%M%S`
    FNAME="/data/in/$EDATE.dat"
    RDATA=`cat /dev/urandom | tr -dc '0-9' | fold -w 256 | head -n 1 | head -c 128`
    echo -n "- `date +%H%M%S` - creating $FNAME"
    echo $RDATA> $FNAME && echo " ."
    sleep 1
done
