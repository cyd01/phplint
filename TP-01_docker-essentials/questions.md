# Formation Docker

## TP 01 : les bases

### Docker store

* trouver l'image du moteur de blogging "ghost"
  * via le site web de docker
  * via la commande docker

### Docker run / les bases

* lancer l'image de ghost de façon à pouvoir accèder au blog
* créer un compte `http://<ip:port>/admin/`
* publier un nouvel article et vérifier qu'il apparait bien
* terminez la commande docker run au moyen d'un docker stop ou kill
* relancez la et vérifiez la page d'acceuil du blog à nouveau. Qu'en concluez vous ?
  * sur la façon dont docker gère les ports ?
  * sur les données dans les containers ?
* lancez la version 0 de ghost. Pouvez vous trouver une différence ?

## Docker run / live configuration

* Lancez mariadb de manière à :
  * que le mot de passe root soit 'capser23'
  * une database nommée 'world' soit crée
  * que le contenu soit celui du fichier fournit `data/world.sql.gz`
  * que l'on puisse y acceder avec le couple user/password `data:pass`

### Docker run / volumes

* Trouvez (via l'aide de docker) un moyen de connecter un répertoire en tant que volume dans un container
* Utilisez l'information ci-dessous pour sauvegarder le répertoire /etc de votre station de travail via :
  * l'image alpine:latest
  * la commande tar
* l'exercice précédent vous donne t'il des idées sur comment contourner le problème précédement rencontré avec l'image ghost ?

