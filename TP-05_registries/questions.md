# TP-05 : docker registries

1. Se connecter a une registry hébergée sur gitlab.com

2. builder l'image fournie dans les sources sous le nom "dockertp5" et le tag "demo"

3. taguer l'image de façon a ce que les informations correspondent à la registry gitlab.com

    - nom de l'image: "formation-docker-basics"
    - tag: "develop"

4. Pusher l'image vers la registry gitlab.com

5. Supprimer les copies locales de cette image

6. Lancer un run avec l'impage de manière à voir comment elle se comporte:

7. Une fois votre image validée taguez la en "stable" et "1.0.0". Qu'en concluez vous ?

# TP-05 : registry locale

1. Lancer l'image officielle `registry:2` en suivant la documentation en ligne
2. Trouvez une façon d'uploader l'image précédente dans cette registry locale
