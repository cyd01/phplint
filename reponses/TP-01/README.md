# Formation Docker

## TP 00 : les bases

### Docker store

* Trouver l'image du moteur de blogging "ghost"
  * via le site web de docker:
    > [https://hub.docker.com/_/ghost](https://hub.docker.com/_/ghost)
  * via la commande docker :
    > ```shell
    > docker search ghost
    > ```
    > Ce qui donne:
    > ```shell
    > NAME                                         DESCRIPTION                                     STARS               OFFICIAL            AUTOMATED
    > ghost                                        Ghost is a free and open source blogging pla…   924                 [OK]
    > ptimof/ghost                                 Allows running Ghost in production mode. Bas…   19                                      [OK]
    > zzrot/alpine-ghost                           Docker Image running Ghost Blogging Service …   16                                      [OK]
    > bitnami/ghost                                Bitnami Docker Image for Ghost                  13                                      [OK]
    > gold/ghost                                   Configurable Golden Ghost image.                9                                       [OK]
    > arm32v7/ghost                                Ghost is a free and open source blogging pla…   6
    > arm64v8/ghost                                Ghost is a free and open source blogging pla…   4
    > devmtl/ghostfire                             Docker Ghost V2 image used on play-with-ghos…   3
    > <snip>
    > ```

### Docker run / les bases

* lancer l'image de ghost de façon à pouvoir accèder au blog

  > ```shell
  > docker run -P ghost
  > ```

* créer un compte http://<ip:port>/admin/ :
  > trivial
* publier un nouvel article et vérifier qu'il apparait bien: 
  > trivial
* terminez la commande docker run au moyen d'un docker stop ou kill

  > ```shell
  > # NB: Ceci stopera tous les containers en cours
  > docker stop $(docker ps -q)
  > ```

* relancez la et vérifiez la page d'acceuil du blog à nouveau. Qu'en concluez vous ?
  * sur la façon dont docker gère les ports ?
  > Les ports sont alloués dynamiquements à cause du '-P'. On peut les forcer via '-p', mais celà est une mauvaise pratique en production.
  * sur les données dans les containers ?
  > Les données d'un containers sont liées à son cycle de vie. Elles disparaissent avec ce dernier.

* lancez la version 0.5 de ghost. Pouvez vous trouver une différence ?
> La page d'admin était noire dans la version 0.5

### Docker run / live configuration

* Lancez mariadb de manière à :
  * que le mot de passe root soit 'capser23'
  * une database nommée 'world' soit crée
  * que le contenu soit celui du fichier fournit `data/world.sql.gz`
  * que l'on puisse y acceder avec le couple user/password `data:pass`

> ```shell
> docker run -it --rm \
>   -v "$(pwd)"/data:/docker-entrypoint-initdb.d:ro \
>   -e MYSQL_ROOT_PASSWORD=capser23 \
>   -e MYSQL_DATABASE=world \
>   -e MYSQL_USER=data \
>   -e MYSQL_PASSWORD=pass \
>   -p 3306:3306 mariadb
> ```

### Docker run / volumes

* Trouvez (via l'aide de docker) un moyen de connecter un répertoire en tant que volume dans un containerdocker run -P ghost

> Utiliser les options _--volume_ ou _-v_

* Utilisez l'information ci-dessous pour sauvegarder le répertoire /etc de votre station de travail via :
  * l'image alpine:latest
  * la commande tar

> ```shell
> docker run -v /etc:/mnt/etc alpine tar -jcf - -C /mnt . > ./etc_backup.tar.bz2
> tar -jtf ./etc_backup.tar.bz2
> ```

* l'exercice précédent vous donne t'il des idées sur comment contourner le problème précédement rencontré avec l'image ghost ?

> Il est possible de persister les donnée de ghost via un simple

* pour aller plus loin, testez le par vous même !

> ```shell
> docker run -v /data/ghost:/var/lib/ghost -P ghost
> ```
