TP 05: docker volumes
========================

1. Utilisez docker-compose pour lancer une stack tirant partie des 2 scripts dans le répertoire src:

cf. docker-compose-step1.yml

2. Couper votre stack aprés l'avoir laissé tourner 5 minutes. Avez vous perdu vos données ? Si non comment les retrouver ? Pouvez vous en faire une sauvegarde ?

Les données ne sont pas perdues, on peut le voir en utilisant la commande suivante :

```shell
docker volume ls
docker volume inspect tp05volumes_worker_1
```

Pour les sauvegarder:

```shell
docker run --volumes-from tp05volumes_worker_1 -v /tmp/:/backup/ alpine:latest tar -zvcf /backup/save.tar.gz /data
```

3. Relancer la même stack en y incluant le script supplémentaire fournit:

cf. docker-compose-step2.yml

4. Pour aller plus loin essayer d'utiliser des data-containers, ce qui est une pratique presque obsolète mais des fois utile.