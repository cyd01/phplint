TP 06: healtchecks
===

1. Créer un container dont on peut savoir l'état en intérogant la page /status

cf. Dockerfile

2. Faire en sorte que ce status soit connu de docker

cf. Dockerfile

3. Vérifier que votre container est bien "healthy"

```shell
docker ps
```

4. Sucharger cela sans toucher au Dockerfile (via compose) de manière à ce que le container ne soit jamais "healthy" très longtemps.

cf. docker-compose.yml

Le container ne redémarre pas totu seul si il est en erreur, c'est le travail d'un orcherstrateur (swarm, rancher, mesos, etc...)
