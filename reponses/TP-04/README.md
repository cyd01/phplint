# Formation Docker

## TP 03 : docker-machine et docker swarm

0. installez docker-machine

    ```shell
    curl -sS -L https://github.com/docker/machine/releases/download/v0.9.0/docker-machine-`uname -s`-`uname -m` -o docker-machine && \
    chmod +x ./docker-machine
    sudo mv ./docker-machine /usr/local/bin/
    ```

1. Provisionnez deux noeuds et un manager sur digital ocean (token fournit par le formateur)

    ```shell
    export DO_TOKEN=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    docker-machine create --driver digitalocean --digitalocean-access-token ${DO_TOKEN} <votre-prenom>-containers-1
    docker-machine create --driver digitalocean --digitalocean-access-token ${DO_TOKEN} --digitalocean-size 2gb <votre-prenom>-containers-2
    docker-machine create --driver digitalocean --digitalocean-access-token ${DO_TOKEN} --digitalocean-size 2gb <votre-prenom>-manager
    docker-machine ls
    ```

2. Notez l'adresse ip du manager

3. Connectez vous au serveur crée pour être le manager

    ```shell
    docker-machine ssh <votre-prenom>-manager
    ```

4. Initialisez le cluster swarm

    ```shell
    docker swarm init --advertise-addr <adresse-ip-manager>
    ```
    Notez la commande affichée dans la sortie.

5. ajouts des noeuds au cluster

    ```shell
    docker-machine ssh <votre-prenom>-container-1
    docker swarm join \
        --token xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx \
        <addr-ip-manager>:2377
    ```
    Recommencez la même chose sur le deuxième serveur.

6. Comment prouver que cela à fonctionné ?

    * docker info
    * docker nodes ls

7. Lancez 1 instance d'un container en tant que service

    * Comment voir ce qui c'est passé ?
        - docker ps sur chaque noeud
        - docker service ls
    * Que constatez vous ?
        - le container est lancé sur le manager

8. Arreter le service et relancez 10 instances du même service

    * Que constatez vous par rapport à la fois d'avant ?
        - les containers vont être schedulés sur tous les noeuds du cluster

9. Arretez tous les services

    ```shell
    docker service rm xxxxxx_xxxxxxxxxx
    ```

10. Lancer 3 services nginx. vérifier que cela fonctionne correctement

    ```shell
    docker service create --replicas 3 -p 80:80 -p 443:443 nginx
    ```

    * testez via un navigateur

11. augmentez le nombre de replicas à 4, que constatez vous ? puis à 10 ? Qu'en concluez vous ?

    ```shell
    docker service scale xxxxxxx_xxxxxx=4
    docker service scale xxxxxxx_xxxxxx=10
    ```
    * On pourrait s'attendre à ce que certaines instances ne fonctionnent pas car le port 80 est occupé. Hors on arrive bien à lancer 4 et 10 instances. Il y a donc quelque chose de fait au niveau réseau. Voir ici : [ingress](https://docs.docker.com/engine/swarm/ingress/)

12. déployer un vrai service - voting-app

    * sur le manager:
        ```shell
        git clone https://github.com/docker/example-voting-app.git
        cd example-voting-app
        docker stack deploy -c docker-stack.yml build
        ```

    * port 8080 : vizualizer
    * port 5000 : votes
    * port 5001 : resultats

13. supprimez les machines sur digital ocean

    ```shell
    docker-machine stop <votre-prenom>-containers-1 <votre-prenom>-containers-2 <votre-prenom>-containers-manager
    docker-machine rm docker-sandbox <votre-prenom>-containers-1 <votre-prenom>-containers-2 <votre-prenom>-manager
    ```
