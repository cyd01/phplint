TP-04: docker registries
=========================

1. Se connecter a une registry hébergée sur gitlab.com

```shell
docker login registry.gitlab.com
```

2. builder l'image fournie dans les sources sous le nom "dockertp4" et le tag "demo"

```shell
docker build -t dockertp4:demo .
```

3. taguer l'image de façon a ce que les informations correspondent à la registry gitlab.com

- nom de l'image: "formation-docker-basics"
- tag: "develop"

```shell
docker tag dockertp4:demo registry.gitlab.com/webofmars/formation-docker-basics:develop
```

4. Pusher l'image vers la registry gitlab.com

```shell
docker push registry.gitlab.com/webofmars/formation-docker-basics:develop
```

5. Supprimer les copies locales de cette image

```shell
docker rmi registry.gitlab.com/webofmars/formation-docker-basics:develop dockertp4:demo
```

ou

```shell
docker image rm registry.gitlab.com/webofmars/formation-docker-basics:develop dockertp4:demo
```

6. Lancer un run avec l'impage de manière à voir comment elle se comporte:

```shell
docker run -i registry.gitlab.com/webofmars/formation-docker-basics:develop
```

7. Une fois votre image validée taguée là en "stable" et "1.0.0". Qu'en concluez vous ?

```shell
docker tag registry.gitlab.com/webofmars/formation-docker-basics:develop registry.gitlab.com/webofmars/formation-docker-basics:stable
docker tag registry.gitlab.com/webofmars/formation-docker-basics:develop registry.gitlab.com/webofmars/formation-docker-basics:1.0.0
docker push registry.gitlab.com/webofmars/formation-docker-basics:stable
docker push registry.gitlab.com/webofmars/formation-docker-basics:1.0.0
```

Les tags sont juste des pointeurs vers des layers quand la layer est déjà existante, le temps de transfert est très rapide.

# TP-05 : registry locale

1. Lancer l'image officielle `registry:2` en suivant la documentation en ligne

```shell
docker run -d -p 5000:5000 -v registry:/var/lib/registry registry:2
```

2. Trouvez une façon d'uploader l'image précédente dans cette registry locale

```shell
docker tag webofmars/getweather localhost:5000/webofmars/getweather
docker push localhost:5000/webofmars/getweather
```