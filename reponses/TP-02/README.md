# Formation Docker

## TP 02 : un site statique

1. construire le container

  > ```shell
  > docker build --no-cache -t webofmars:tp1 .
  > ```

2. quel est le nom du container ? comment rendre ce nom plus identifiable ?

  > choisi au hasard
  > lancer `docker run` avec `--name xxxxxxxx`

3. sur quel port écoute le service web ? comment faire pour résoudre ce problème ?

  > par défaut aucun !
  > lancer docker run avec au choix "-P (ports dynamiques)" ou "-p 8080:80 (port fixé)"

4. afficher la page

  > ```shell
  > docker run --rm -d --name tp1_web -p 8080:80 webofmars:tp1
  > aller à http://localhost:8080
  > ```

5. Faire en sorte que l'image pèse moins de 145 Mo

  > il existe plusieurs options:
    > * utiliser l'image alpine (moins grosse):
      > ```shell
      > docker image ls httpd
      > REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
      > httpd               alpine              cdacf701b255        3 days ago          97.5MB
      > httpd               latest              e40405f80704        11 days ago         132MB
      > httpd               2.4-alpine          dfd436f9a5d8        6 weeks ago         91.8MB
      > ```

    > * Utiliser également le fichier .dockerignore (voir sources solutions)
    > * Soyez séléctif dans les instructions `COPY|ADD` du `Dockerfile`

6. verifier si le container tourne encore

  > ```shell
  > docker ps -a
  > ```

7. arreter le container

```shell
docker stop tp1_web ou docker stop $(docker ps -q)
```

8. lister les images

```shell
docker images
```

9. supprimer son image

```shell
docker rmi webofmars:tp1
```
