# Docker Formation D01

## TP02 : docker-compose & stacks (aka services)

1. puller les images

```shell
echo -e "nginx:latest\nphp:5.6.30-fpm-alpine\nphp:7-fpm-alpine\nmysql:5.7.17\nmariadb:\nredis:3.0.7-alpine" | xargs -L 1 docker pull
```

2. construire une page html statique + une page php type "hello world"

* voir le dossier sources

3. créer un fichier docker-compose.yml (version 1) représentant une stack comme suit:

* voir fichier docker-compose-v1.yml

4. lancer la stack

```shell
docker-compose -f docker-compose-v1.yml -p tp2 up
```

5. mettre les sessions dans redis

* ajouter le code redis dans index.php
* créer un fichier entrypoint.sh de surcharge pour le container php-fpm (install pecl redis)
* surcharger l'entrypoint dans le fichier docker-compose.yml
* relancer la stack

6. stocker en base de donnée l'id session et l'heure de démarrage de la session
* ajouter le code mysqli dans index.php
* modifier le fichier entrypoint.sh de surcharge pour le container php-fpm (install native mysqli extension)
* relancer la stack

7. comment visualiser les sessions dans redis ?

```shell
docker run -d --rm -P --link tp2_sessions_1:redis tenstartups/redis-commander --redis-host redis
```

8. comment visualiser les donées dans MySQL ?

```shell
docker run -d --rm -P --link tp2_database_1:tp2db clue/adminer
```

### Pour aller plus loin :

* migrer la BDD de MySQL à MariaDB
	- remplacer l'image MySQL dans le docker-compose.yml

* tester si compatible PHP 7 ?
	- remplacer l'image PHP dans le docker-compose.yml

* utiliser les data volumes pour stocker les infos
	- au lieu d'utiliser les bindings ajouter des images busybox qui ne démareront qu'une seule fois avec les datas
	- ou créer des volumes avec docker volumes et les positionner en external dans le docker-compose.yml

* convertir en dockerfile v2 et/ou v3
	- ajout de la section version
	- ajout de la section networks
	- ajout de la section volumes

* comment limiter le temps de démarrage du container PHP ?
	- faire un dockerfile qui hérite de php-fpm est le builder (soit via docker-compose, soit via une registry)