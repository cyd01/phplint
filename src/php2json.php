<?php

echo "MYSQL_HOST: ".getenv('MYSQL_HOST').PHP_EOL;
echo "MYSQL_PORT: ".getenv('MYSQL_PORT').PHP_EOL;
echo "MYSQL_USER: ".getenv('MYSQL_USER').PHP_EOL;
echo "MYSQL_PASSWORD: ".getenv('MYSQL_PASSWORD').PHP_EOL;
echo "MYSQL_DATABASE: ".getenv('MYSQL_DATABASE').PHP_EOL;
echo "MYSQL_TABLE: ".getenv('MYSQL_TABLE').PHP_EOL;

$link = mysqli_connect(getenv('MYSQL_HOST').':'.getenv('MYSQL_PORT'), 
    getenv('MYSQL_USER'), 
    getenv('MYSQL_PASSWORD'), 
    getenv('MYSQL_DATABASE'));

if (!$link) {
    echo "Error: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

echo "Success: A proper connection to MySQL was made!".PHP_EOL;
echo "Host information: ".mysqli_get_host_info($link).PHP_EOL;

$query = 'SELECT * FROM '.getenv('MYSQL_TABLE').';';

if ($result = $link->query($query)) {

    /* fetch object array */
    while ($obj = $result->fetch_object()) {
        printf ("%s\n", json_encode($obj));
    }

    /* free result set */
    $result->close();
}

mysqli_close($link);
