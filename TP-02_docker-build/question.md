# Formation Docker

## TP 02 : un site statique

1. Construire un container permettant d'afficher une page web statique (fournie dans le répertoire _sources_)
2. quel est le nom du container ? comment rendre ce nom plus identifiable ?
3. sur quel port écoute le service web ? comment faire pour résoudre ce problème ?
4. afficher la page
5. Faire en sorte que l'image pèse moins de 150 Mo
6. verifier si le container tourne encore
7. arreter le container
8. lister les images
9. supprimer son image

### Pour aller plus loin

* Publier l'image sur le docker store
* Rendre le build de l'image automatique

## TP 02: supervisord

1. Intégrer supervisord dans le container précédent de manière à pouvoir lancer une tâche asynchrone injectant de la donnée dans la page. (astuce: utilisez par exemple sed sur le fichier index.html)