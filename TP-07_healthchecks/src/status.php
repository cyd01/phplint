<?php

switch ($_GET['status']) {
    case 'false':
        header("X-MYAPP-STATUS: false",true,503);
        break;
    case 'true':
        header("X-MYAPP-STATUS: true",true,200);
        break;
    case 'random':
        $values = [200, 404, 500, 502, 503];
        $i = random_int(0, 4);
        header("X-MYAPP-STATUS: random",true,$values[$i]);
        break;
    case '';
        header("X-MYAPP-STATUS: true",true,200);
        break;
    default:
        header("X-MYAPP-STATUS: true",true,$_GET['status']);
        break;
}
