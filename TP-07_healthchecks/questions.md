# TP 06 : healtchecks

1. Créer un container dont on peut savoir l'état en intérogant la page /status

2. Faire en sorte que ce status soit connu de docker

3. Vérifier que votre container est bien "healthy"

4. Sucharger cela sans toucher au Dockerfile (via compose) de manière à ce que le container ne soit jamais "healthy" très longtemps.

_N.B_: penser à utiliser 'watch docker ps' & surveiller les ids de containers.

Qu'en concluez vous ?
