# TP 08 : workflow

1. Trouver un moyen de teser le code fournit dans le répertoire src/ en utilisant docker:
- lint
- functional test

2. En utilisant gitlab faire un fichier de config CI/CD (.gitlab-ci.yml) qui permet de tester le code de la même manière qu'au dessus

3. builder une image stable avec gitlab

4. publier l'image stable dans la registry gitlab

