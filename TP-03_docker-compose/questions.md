# Docker Formation D01

## TP03 : docker-compose & stacks (aka services)

1. puller les images necessaires
	* nginx:latest
	* php:5.6.30-fpm-alpine
	* php:7-fpm-alpine
	* mysql:5.7.17
	* mariadb:
	* redis:3.0.7-alpine

2. construire une page html statique + une page php type "hello world"

3. créer un fichier docker-compose.yml (version 1) représentant une stack comme suit:
	* un serveur nginx
	* un serveur php-fpm
	* un serveur redis (pour les sessions)
	* un server MySQL pour les datas

4. lancer la stack

5. mettre les sessions dans redis

6. stocker en base de donnée l'id session et l'heure de démarrage de la session

7. comment visualiser les sessions dans redis ?

8. comment visualiser les donées dans MySQL ?

### Pour aller plus loin :

* migrer la BDD de MySQL à MariaDB
* tester si compatible PHP 7 ?
* utiliser les data volumes pour stocker les infos
* convertir en dockerfile v2 et/ou v3
* comment limiter le temps de démarrage du container PHP ?
