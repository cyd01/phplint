DROP TABLE IF EXISTS sessions;
CREATE TABLE sessions(id char(40), timestamp DATETIME);
INSERT INTO sessions(id,timestamp) VALUES (11111111, NOW());
INSERT INTO sessions(id,timestamp) VALUES (22222222, NOW());
INSERT INTO sessions(id,timestamp) VALUES (33333333, NOW());
INSERT INTO sessions(id,timestamp) VALUES (44444444, NOW());
INSERT INTO sessions(id,timestamp) VALUES (55555555, NOW());
