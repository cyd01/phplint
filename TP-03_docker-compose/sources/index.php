<?php 
	session_start(); 
	$SESSID = session_id();
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Un peu plus compliqué</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="css/freelancer.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">
<!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="#page-top">Le PHP c'est plus ... dynamique !</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        <a href="#portfolio">Portfolio</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#about">About</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#contact">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <img class="img-responsive" src="img/profile.png" alt="">
                    <div class="intro-text">
						<span class="name">Hello world in PHP <?php echo phpversion() ?></span>
						<hr class="star-light">
					</div>
				</div>
			</div>
		</div>
	</header>

<?php $count = isset($_SESSION['count']) ? $_SESSION['count'] : 1; ?>

<section id="redis">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2>Redis sessions</h2>
				<hr class="star-primary">
				<div>Nombre de vos visites ou refresh de la page : <?php echo $count; $_SESSION['count'] = ++$count; ?></div>
			</div>
		</div>
	</div>
</section>

<section id="MySQL">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2>MySQL Sessions Logging</h2>
				<hr class="star-primary">
				<div>
					Les 10 dernières sessions loguées dans la base des données:<br />
					<?php

						$mysqli = new mysqli("database", "dev", "dev", "formation");

						if ($mysqli->connect_errno) {
	    					echo "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
						}

						if (!$mysqli->query("INSERT INTO sessions(id,timestamp) VALUES (\"$SESSID\", CURRENT_TIMESTAMP())")) {
						    echo "Echec lors de l'insertion de données : (" . $mysqli->errno . ") " . $mysqli->error;
						}
					?>
					&nbsp;
					<table border="1" align="center">
						<tr><th align="center">ID</th><th align="center">timestamp</th></tr>
						<?php 
								$results = $mysqli->query("SELECT id, timestamp from sessions ORDER BY timestamp DESC LIMIT 10;");
								while($row = $results->fetch_assoc()) {
    								echo '<tr><td>';
    								echo $row['id'].'</td><td>';
    								echo $row['timestamp'].'</td></tr>';
								}
						?>
					</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


	</body>
</html>
