#!/bin/ash

apk update > /dev/null
apk add autoconf build-base gcc

pecl install redis > /dev/null

docker-php-ext-install mysqli > /dev/null

docker-php-entrypoint -F