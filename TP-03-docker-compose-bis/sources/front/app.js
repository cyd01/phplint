function onTaskBtn(event) {
    console.log("TaskBtn was clicked: " + event);
    let t = { "taskName": event, "createdOn": Date.now() };
    createTask(t);
}

function getTasks(callback) {
  console.log("Getting task list")
  var tasksList = {};
  var XHR = new XMLHttpRequest();

  // Définissez ce qui se passe en cas de succès de soumission de données
  XHR.addEventListener('load', function(event) {
    console.log('Ouais ! Données envoyées et réponse chargée.');
  });

  // Définissez ce qui arrive en cas d'erreur
  XHR.addEventListener('error', function(event) {
    console.log('Oups! Quelque chose s\'est mal passé.');
  });

  XHR.onreadystatechange = function(tasksList) {
    if (XHR.readyState == XMLHttpRequest.DONE) {
        tasksList = XHR.responseText;
        console.log("Tasks: " + JSON.stringify(tasksList))
        callback(tasksList)
    }
  }

  // Configurez la requête
  XHR.open('GET', 'http://127.0.0.1.nip.io:3000/tasks');

  // Ajoutez l'en-tête HTTP requise pour requêtes POST de données de formulaire
  //XHR.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  XHR.setRequestHeader("Content-Type", "application/json");

  // Finalement, envoyez les données.
  XHR.send();
}

function createTask(data) {
  var XHR = new XMLHttpRequest();

  // Définissez ce qui se passe en cas de succès de soumission de données
  XHR.addEventListener('load', function(event) {
    console.log('Ouais ! Données envoyées et réponse chargée.');
    location.reload(true);
  });

  // Définissez ce qui arrive en cas d'erreur
  XHR.addEventListener('error', function(event) {
    console.log('Oups! Quelque chose s\'est mal passé.');
  });

  // Configurez la requête
  XHR.open('POST', 'http://127.0.0.1.nip.io:3000/tasks');

  // Ajoutez l'en-tête HTTP requise pour requêtes POST de données de formulaire
  //XHR.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  XHR.setRequestHeader("Content-Type", "application/json");

  // Finalement, envoyez les données.
  XHR.send(JSON.stringify(data));
}
