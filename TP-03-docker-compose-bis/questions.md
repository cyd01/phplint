# Docker Formation

## TP03 : docker-compose & stacks (aka services)

1. puller les images necessaires
   * nginx (ou httpd)
   * mongodb
   * nodejs (version: 11-stretch)

2. builder les 2 containers présents dans les sources (app & api)

3. créer un fichier docker-compose.yml (version 1) représentant une stack comme suit:
   * un serveur web statics : nginx ou httpd 2.4
   * un serveur applicatif nodejs
   * un serveur mongodb pour le stockage des datas

Vue d'ensemble: ![schema](schema.png)

4. lancer la stack

### Pour aller plus loin :

* comment visualiser les datas dans mongodb ?
* comment sauvegarder les donées dans mongodb ?
* tester si l'application est compatible node 8.15 ?