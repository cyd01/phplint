# Formation Docker

## TP 03 : docker-machine et docker swarm

0. installez docker-machine

1. Provisionnez deux noeuds et un manager sur digital ocean (token fournit par le formateur)

    * _votre-prenom_-manager
    * _votre-prenom_-containers-1
    * _votre-prenom_-containers-2

2. Notez l'adresse ip du manager

3. Connectez vous au serveur crée pour être le manager

4. Initialisez le cluster swarm

5. ajouts des noeuds au cluster :

    * Ajouter le 1er noeud au cluster
    * Recommencez la même chose sur le deuxième serveur.

6. Comment prouver que cela à fonctionné ?

7. Lancez 1 instance d'un container en tant que service

    * le plus simple : alpine ping <site web>
    * Comment voir ce qui c'est passé ?
    * Que constatez vous ?

8. Arreter le service et relancez 10 instances du même service

    * Que constatez vous par rapport à la fois d'avant ?

9. Arretez tous les services

10. Lancer 3 services nginx. vérifier que cela fonctionne correctement

11. augmentez le nombre de replicas à 4, que constatez vous ? puis à 10 ? Qu'en concluez vous ?

12. déployer un vrai service - voting-app

    * trouvez là sur le store ou sur github
    * déployez cette stack
    * que constatez vous ?

13. supprimez les machines sur digital ocean

    ```shell
    docker-machine stop _votre-prenom_-containers-1 _votre-prenom_-containers-2 _votre-prenom_-containers-manager
    docker-machine rm docker-sandbox _votre-prenom_-containers-1 _votre-prenom_-containers-2 _votre-prenom_-manager
    ```
